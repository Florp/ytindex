from sys import argv
from app.tasks import update
from app.index import check_index

check_index()  # Create the index if it doesn't exist yet

if argv[1] == 'update':
    update()
