import os
# noinspection PyPackageRequirements
from dotenv import load_dotenv

# Load .env file
BASEDIR = os.path.abspath(os.path.dirname(__file__))
load_dotenv(os.path.join(BASEDIR, '.env'))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY')
    ELASTICSEARCH_URL = os.environ.get('ELASTICSEARCH_URL')
    DOWNLOAD_THREADS = os.environ.get('DOWNLOAD_THREADS')
    SUB_LANG = os.environ.get('SUB_LANG')
