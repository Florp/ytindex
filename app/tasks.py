import youtube_dl
from app.ydl import YDLLogger, ydl_opts, ydl_hook
from app.index import update_index
from app.config import Config
from threading import Thread
from collections import deque
from urllib import request


class VideoFetcher:
    # TODO: clean up

    def __init__(self, url, workers=None):
        self.url = url
        self.ydl_opts_flat = ydl_opts
        self.ydl_opts_flat['extract_flat'] = True

        if workers is None:
            if Config.DOWNLOAD_THREADS is not None:
                self.worker_amount = Config.DOWNLOAD_THREADS
            else:
                self.worker_amount = 50
        else:
            self.worker_amount = workers

        self.all_meta = []  # All videos

    def start(self):
        # Get URLs
        with youtube_dl.YoutubeDL(self.ydl_opts_flat) as ydl:
            info_dict = ydl.extract_info(self.url, download=False)

        # Change url to playlist type if channel type was given
        if info_dict['extractor'] == 'youtube:channel' or info_dict['extractor'] == 'youtube:user':
            self.url = info_dict['url']

            # Restart
            self.start()
            return

        # Build queue
        if info_dict['extractor'] == 'youtube:playlist':
            self.video_queue = deque(info_dict['entries'])
        else:
            self.video_queue = deque(info_dict)

        # Start workers
        workers = []
        for i in range(self.worker_amount):
            worker = Thread(target=self.__fetch_info_worker__)
            worker.start()
            workers += [worker]

        # Wait until all workers are done
        for worker in workers:
            worker.join()

        update_index(self.all_meta)

    def __fetch_info_worker__(self):
        while True:
            try:
                with youtube_dl.YoutubeDL(ydl_opts) as ydl:
                    video = self.video_queue.popleft()
                    print(len(self.video_queue))  # Print the amount of videos left in the queue

                    full_meta = ydl.extract_info(video['url'], download=False)
                    full_meta['automatic_captions'] = self.get_subs(full_meta['automatic_captions'])
                    self.all_meta += [full_meta]
            except IndexError:
                break

    @staticmethod
    def get_subs(subs_meta):
        # TODO: add support for multiple subtitle languages
        if len(subs_meta) > 0:
            url = subs_meta[Config.SUB_LANG][0]['url']  # Get URL from meta
            subs = request.urlopen(url).read().decode('utf-8')  # Download subs
            subs = subs[subs.find('<p'):]  # Remove starting bit
            return subs
