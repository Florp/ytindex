from elasticsearch import Elasticsearch, client
from elasticsearch import helpers
from app.config import Config

es = Elasticsearch(Config.ELASTICSEARCH_URL)

# YTDL fields irrelevant for us
exclude_fields = [
    '_type',
    'thumbnails',  # Same as 'thumbnail'
    'annotations',  # No longer provided by YouTube

    # Irrelevant
    'is_live',  # Won't be up to date
    'start_time',  # For when using timecodes
    'end_time',
    'playlist',  # For video+playlist player
    'playlist_index',

    # Download related
    'extractor_key',
    'format',
    'extractor',
    'formats',
    'requested_formats',
    'requested_subtitles',
    'format_id',
    'width',
    'height',
    'resolution',
    'fps',
    'vcodec',
    'vbr',
    'stretched_ratio',
    'acodec',
    'abr',
    'ext',

    # Duplicate or derivative
    'id',
    'display_id',
    'channel_url',
    'uploader_url',
    'webpage_url',
    'webpage_url_basename',
    'subtitles'
]


def check_index():
    """Check if index exists and create if not"""

    ic = client.IndicesClient(es)
    if not ic.exists('ytindex'):
        res = ic.create(
            'ytindex',
            {
                'settings': {
                    'index': {

                    },
                    'analysis': {
                        'analyzer': {
                            'htmlStripAnalyzer': {
                                'type': 'custom',
                                'tokenizer': 'standard',
                                'filter': ['standard', 'lowercase'],
                                'char_filter': ['html_strip']
                            }
                        }
                    }
                },
                'mappings': {
                    'video': {
                        'properties': {
                            'upload_date': {
                                'type': 'date',
                                'format': 'basic_date'
                            },
                            'automatic_captions': {
                                'type': 'text',
                                'analyzer': 'htmlStripAnalyzer'
                            }
                        }
                    }
                }
            }
        )
        if not res['acknowledged']:
            print('Something went wrong while creating the index')


def update_index(videos):
    """
    Add or update videos in the index
    :param videos: the list of videos dictionary
    :return: the result
    """

    # Remove unneeded fields and build actions
    actions = []
    for video in videos:
        id = video['id']
        for key in exclude_fields:
            if key in video:
                del video[key]

        actions += [{
            '_index': 'ytindex',
            '_type': 'video',
            '_id': id,
            '_source': video
        }]

    res = helpers.bulk(es, actions)
    es.indices.refresh(index="ytindex")
    return res
