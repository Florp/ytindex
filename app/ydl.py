# Youtube-DL logging and options


class YDLLogger(object):
    def debug(self, msg):
        pass
        # print(msg)

    def warning(self, msg):
        print(msg)

    def error(self, msg):
        print(msg)


def ydl_hook(d):
    if d['status'] == 'finished':
        print('Done downloading, now converting ...')


# Options for youtube_dl. Subtitles are found by extracting info
ydl_opts = {
    'logger': YDLLogger(),
    'progress_hooks': [ydl_hook],
    'noplaylist': True,  # When in doubt, fetch the video instead of the playlist

    # Subtitle stuff
    'writesubtitles': True,
    'writeautomaticsub': True,
    'allsubtitles': True
}
